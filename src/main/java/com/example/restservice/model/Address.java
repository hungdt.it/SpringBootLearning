package com.example.restservice.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Address implements Serializable {

    private static final long serialVersionUID = -7680282508808725530L;
    private String street;
    private String suite;
    private String city;
    private String zipcode;
    private Geo geo;
}
