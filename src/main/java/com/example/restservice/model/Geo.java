package com.example.restservice.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Geo implements Serializable {

    private static final long serialVersionUID = 8934576147823838379L;
    private String lat;
    private String lng;
}
