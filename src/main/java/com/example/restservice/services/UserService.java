package com.example.restservice.services;

import com.example.restservice.model.User;

import java.util.List;

public interface UserService {
    List<User> getAllUserFromJsonFile();

    User getUserById(Integer id);

    List<User> getAllUserFromRestAPI();

    User getUserByIdFromRestAPI(Integer id);
}
