package com.example.restservice.services;

import com.example.restservice.model.User;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.io.InputStream;
import java.util.List;

@Service
@Slf4j
public class UserServiceImpl implements UserService {

    @Value("${user.file.path}")
    private String userJsonFileName;

    @Value("${url.user.test}")
    private String userURL;

    @Override
    public List<User> getAllUserFromJsonFile() {
        log.info("Get all User");
        return findAllUser();
    }

    @Override
    public User getUserById(Integer id) {
        log.info("Get User By Id: {}", id);
        List<User> users = findAllUser();

        User user = new User();
        for(int i=0; i< users.size(); i++) {
            if(users.get(i).getId().intValue() == id.intValue()) {
                user = users.get(i);
                break;
            }
        }
        log.info("Response data: {}", user);
        return user;
    }

    @Override
    public List<User> getAllUserFromRestAPI() {
        log.info("Get All user from rest API: {}", userURL);
        RestTemplate restTemplate = new RestTemplate();
        ResponseEntity<List<User>> responseEntity = restTemplate.exchange(userURL, HttpMethod.GET, null, new ParameterizedTypeReference<List<User>>() {});
        return responseEntity.getBody();
    }

    @Override
    public User getUserByIdFromRestAPI(Integer id) {
        log.info("Get user by Id {} from rest API: {}", id, userURL);
        StringBuilder urlBuilder = new StringBuilder(userURL).append("/").append(id);

        RestTemplate restTemplate = new RestTemplate();

        User user = restTemplate.getForObject(urlBuilder.toString(), User.class);
        log.info("Response data: {}", user);
        return user;
    }

    public List<User> findAllUser() {
        ObjectMapper objectMapper = new ObjectMapper();
        TypeReference<List<User>> typeReference = new TypeReference<List<User>>(){};
        InputStream inputStream = TypeReference.class.getResourceAsStream(userJsonFileName);
        try {
            List<User> users = objectMapper.readValue(inputStream, typeReference);
            log.info("List user: {}", users);

            return users;
        } catch (Exception e) {
            log.error("Error: {}", e);
        }
        return null;
    }
}
